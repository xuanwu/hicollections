
# hicollections

提供C风格的容器实现，包括List/RbTree/AvlTree. 他们共同的特点是容器本身不分配任何内存，在某些场景中有自身的优势.

容器支持Send，但任何元素挂接到集合后，元素本身就不能支持所有权转移和覆写, 当然元素是不支持Send的.

BtreeMap/BTreeSet可以定制内部节点的内存分配策略.

# 修改记录

1. v0.1.5版本
> 集成hipool v0.3版本
1. v0.1.4 版本
> 新增List::move_after, List::move_before,List::add_before
1. v0.1.3 版本
> 新增`List::add_after`
