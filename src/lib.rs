//!  C-liked Collections: List/RbTree/AvlTree.
//! List/RbTree/AvlTree本身不分配任何内存，也支持Send，但任何元素挂接到集合后，就不能支持转移和覆写, 元素是不支持Send的.
//! BtreeMap/BTreeSet可以定制内部节点的内存分配策略.
//!

#![no_std]

use core::marker::PhantomData;
use core::ptr;
use core::mem::size_of;

mod list;
pub use list::{List, ListNode};

mod rbtree;
pub use rbtree::{RbTree, RbTreeNode};

mod avltree;
pub use avltree::{AvlTree, AvlTreeNode};

mod btree;
pub use btree::{BTreeMap, BTreeSet};

/// 增加到容器后的返回值类型，其生命周期参数为加入元素的生命周期.
/// 在其生命周期内避免获取加入元素的可写引用.
pub struct Link<'a> {
    mark: PhantomData<&'a ()>,
}

impl<'a> Link<'a> {
    pub(crate) const fn new() -> Self {
        Self { mark: PhantomData }
    }

    pub fn drop(self) {}
}

///
/// 内部涉及到指针的转换操作，当前指针操作只能在运行期间成, 不支持const
///
fn node_offset<T, R, F>(f: F) -> usize
where
    F: FnOnce(*const T) -> *const R,
{
    let base = ptr::null::<T>();
    let base_addr = base as usize;
    let node_addr = f(base) as usize;
    debug_assert!(
        node_addr >= base_addr && node_addr + size_of::<R>() <= base_addr + size_of::<T>()
    );
    node_addr - base_addr
}

